var firebaseConfig = {
  apiKey: "AIzaSyCkpFlsELS9thltWJsKPXxJczABneIp55E",
  authDomain: "clouds-lab-5-8e96e.firebaseapp.com",
  databaseURL: "https://clouds-lab-5-8e96e-default-rtdb.firebaseio.com",
  projectId: "clouds-lab-5-8e96e",
  storageBucket: "clouds-lab-5-8e96e.appspot.com",
  messagingSenderId: "285339484794",
  appId: "1:285339484794:web:f4918c3601647f6dae31c3"
};


function login() {
    firebase.auth().signInWithPopup(provider).then(res => {
        location.href = "/main_app.html"
    }).catch(e => {
        console.log(e)
    })
}

function refresh_login() {
    firebase.auth().onAuthStateChanged(user => {
        if (user) {
            location.href = "/main_app.html"
        }
    })
}

firebase.initializeApp(firebaseConfig);

let provider = new firebase.auth.GoogleAuthProvider()
document.getElementById('login').addEventListener('click', login)

refresh_login()

