// Sign in again
var viewer;
var movie_id_u = [];

var firebaseConfig = {
    apiKey: "AIzaSyCkpFlsELS9thltWJsKPXxJczABneIp55E",
    authDomain: "clouds-lab-5-8e96e.firebaseapp.com",
    databaseURL: "https://clouds-lab-5-8e96e-default-rtdb.firebaseio.com",
    projectId: "clouds-lab-5-8e96e",
    storageBucket: "clouds-lab-5-8e96e.appspot.com",
    messagingSenderId: "285339484794",
    appId: "1:285339484794:web:f4918c3601647f6dae31c3"
  };
firebase.initializeApp(firebaseConfig);
let provider = new firebase.auth.GoogleAuthProvider()

function Remove(val) {
    const db = firebase.firestore()
    db.collection('users').get(viewer.email).then((snapshot) => {
        let old_ids = snapshot.docs[0].data()['movie_ids']
        old_ids = old_ids.filter(item => item !== val)
        console.log(old_ids)
        db.collection('users').doc(viewer.email).set({
            movie_ids: old_ids
        }).then(() => {
            window.location.reload()
        });

    })
}

authen_State()
document.getElementById('wishlist-btn').addEventListener('click', OpenWishList)
document.getElementById('logout-btn').addEventListener('click', Logout)
document.getElementById('menu-btn').addEventListener('click', ReturnToMenu)

database_rec()



function Logout() {
    firebase.auth().signOut().then(() => {
        location.href = "/index.html"
    }).catch(e => {
        console.log(e)
    })
}

function authen_State() {
    firebase.auth().onAuthStateChanged(user => {
        viewer = user
        document.getElementById('username-btn').innerHTML = viewer.displayName
    })
}

function database_rec() {
    const rt_database = firebase.firestore()

    rt_database.collection('users').get().then((snapshot) => {
        let ID = 0
        
        for (let i = 0; i < snapshot.docs.length; i++) {
            let docu = snapshot.docs[i].id
            
            let doc_data = snapshot.docs[i].data()
            
            let u_email = viewer.email
            
            if (u_email == docu) {
                ID = 1                
                movie_id_u = doc_data['movie_ids']

            }
        }
        if (ID == 0) {
            rt_database.collection('users').doc(viewer.email).set({
                movie_ids: [],
            });
        }

        WishList()

    })
}

function search(input) {
    var er;
    var a;
    var j;
    var Text;
    var filter = input.value.toUpperCase();
    er = document.getElementsByClassName("movie");
    for (j = 0; j < er.length; j++) {
        a = er[j];
        Text = a.innerText;
        if (Text.toUpperCase().indexOf(filter) > -1) {
            er[j].style.display = "";
        } else {
            er[j].style.display = "none";
        }
    }
}


function AddToWishList(ID) {
    const rt_database = firebase.firestore()
    rt_database.collection('users').get(viewer.email).then((snapshot) => {
        let old_ids = snapshot.docs[0].data()['movie_ids']
        old_ids.push(ID)
        rt_database.collection('users').doc(viewer.email).set({
            movie_ids: old_ids
        }).then(() => {
            window.location.reload()
        });
    })
}

function OpenWishList() {
    location.href = "/wish_list.html"
}

function ReturnToMenu(){
    location.href = "/main_app.html"
}

function WishList() {
    firebase.database().ref('movies-list').once('value', function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();
            var node = document.createElement("Button");
            node.innerHTML = "Title: " + childData.title + " ||| Year: " + childData.year + " ||| Genre: " + childData.genre
            node.id = childData.id
            node.addEventListener('click', () => {
                Remove(node.id)
            })
            if (movie_id_u.includes(node.id)) {
                document.getElementById('movies').appendChild(node)
            } else {

            }
        });
    });
}





